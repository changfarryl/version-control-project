/*

  Author: Farryl Chang, Cara Wang
  
  This class is used to create the catblob pet.

*/

class Pet{
  
  // Properties
  private int hunger; // Stores the pet's hunger value
  private int age; // Stores the pet's age as in-game days
  private int state; // Stores the pet's current state; 0 - idle, 1 - being fed
  private int fedAnimationTimer; // Controls the timing for the pet's fed animation
  
  // Stores the animation images for the pet
  private PImage[] cat1 = new PImage [9]; // Cat's regular pose
  private PImage[] cat2 = new PImage [13]; // Cat's pose after being fed
  
  // Pet constructor
  Pet(int hunger, int age){
    
    // Initialize properties
    setHunger(hunger);
    setAge(age);
    setState(0);
    fedAnimationTimer = 0;
  
    // Load the cat's idle animation images
    cat1[0] = loadImage("Assets/c1-1.png");
    cat1[1] = loadImage("Assets/c1-2.png");
    cat1[2] = loadImage("Assets/c1-3.png");
    cat1[3] = loadImage("Assets/c1-4.png");
    cat1[4] = loadImage("Assets/c1-5.png");
    cat1[5] = loadImage("Assets/c1-6.png");
    cat1[6] = loadImage("Assets/c1-7.png");
    cat1[7] = loadImage("Assets/c1-8.png");
    cat1[8] = loadImage("Assets/c1-9.png");
    
    // Load the cat's fed animation images
    cat2[0] = loadImage("Assets/c2-1.png");
    cat2[1] = loadImage("Assets/c2-2.png");
    cat2[2] = loadImage("Assets/c2-3.png");
    cat2[3] = loadImage("Assets/c2-4.png");
    cat2[4] = loadImage("Assets/c2-5.png");
    cat2[5] = loadImage("Assets/c2-6.png");
    cat2[6] = loadImage("Assets/c2-7.png");
    cat2[7] = loadImage("Assets/c2-8.png");
    cat2[8] = loadImage("Assets/c2-9.png");
    cat2[9] = loadImage("Assets/c2-10.png");
    cat2[10] = loadImage("Assets/c2-11.png");
    cat2[11] = loadImage("Assets/c2-12.png");
    cat2[12] = loadImage("Assets/c2-13.png");
    
  }
  
  // Setters and getters
  void setHunger(int hunger){
    if(hunger < 0) // Minimum value of 0
      this.hunger = 0;
    else if(hunger > 100) // Maximum value of 100
      this.hunger = 100;
    else
      this.hunger = hunger;
  }
  
  int getHunger(){
    return hunger;
  }
  
  void setAge(int age){
    if(age < 1) // Minimum value of 1
      this.age = 1;
    else
      this.age = age;
  }
  
  int getAge(){
    return age;
  }
  
  void setState(int state){
    if(state < 0 || state > 1) // State can only be between 0 and 1
      this.state = 0; // Default value 0
    else
      this.state = state;
  }
  
  int getState(){
    return state;
  }
  
  // Updates the pet
  void update(){
    
    // Decrement the fed animation timer
    if(fedAnimationTimer > 0)
      fedAnimationTimer --;
      
    if(fedAnimationTimer == 0 && getState() != 0)
      setState(0);
    
  }
  
  // Displays the pet
  void display(){
    
    // Set image mode to center
    imageMode(CORNER);
    
    // Cat's regular pose
    if(state == 0)
      image(cat1[(frameCount / 5) % 8],280,200,350,350);
      
    // Cat's pose after being fed
    else if(state == 1)
      image(cat2[(frameCount / 5) % 13],200,240,350,350);
  }
  
  // Feeds the pet
  void eat(){
    
    // Decrease hunger and set fed animation timer
    setHunger(getHunger() - 1);
    setState(1);
    fedAnimationTimer = 120;
    
  }
  
}
