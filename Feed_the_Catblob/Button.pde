/*

  Author: Farryl Chang
  
  This class is used to create buttons that will perform various actions in the game.

*/

class Button{
  
  // Properties
  private PVector topLeft, bottomRight; // Stores the position of the button based on its top left and bottom right vertices
  private PImage icon; // Stores the image that will visually represent the button
  private int buttonWidth, buttonHeight; // Stores the width and height of the button
  
  // Button constructor
  Button(int x1, int y1, int x2, int y2, PImage icon, int buttonWidth, int buttonHeight){
    
    topLeft = new PVector(x1, y1);
    bottomRight = new PVector(x2, y2);
    this.icon = icon;
    this.buttonWidth = buttonWidth;
    this.buttonHeight = buttonHeight;
    
  }
  
  // Setters and getters
  void setTopLeft(PVector topLeft){
    this.topLeft = topLeft;   
  }
  
  PVector getTopLeft(){
    return topLeft;
  }
  
  void setBottomRight(PVector bottomRight){
    this.bottomRight = bottomRight;
  }
  
  PVector getBottomRight(){
    return bottomRight;
  }
  
  void setIcon(PImage icon){
    this.icon = icon;
  }
  
  PImage getIcon(){
    return icon;
  }
  
  // Displays the button icon
  void display(){
    
    // Displays the button icon (enlargen if player is hovering over it)
    imageMode(CORNERS);
    if(mouseCheck())
      image(icon, topLeft.x - 10, topLeft.y - 10, bottomRight.x + 10, bottomRight.y + 10);
    else
      image(icon, topLeft.x, topLeft.y, bottomRight.x, bottomRight.y);
    
  }
  
  // Checks whether the mouse is being clicked on
  boolean mouseCheck(){
    
    // The button is clicked if the mouse if within the button's bounds
    if(mouseX >= topLeft.x && mouseX <= bottomRight.x && mouseY >= topLeft.y && mouseY < bottomRight.y)
      return true;
    else
      return false;
    
  }
  
}
