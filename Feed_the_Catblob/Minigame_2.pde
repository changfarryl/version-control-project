/*
Minigame 2: Whack-a-Mouse

Author: Rei Zhang

click as many mice as you can within the time limit!

*/
class Minigame_2{

float miceX; //mice positions
float miceY;

int rng; //randomly spawns mice in any of the six holes
int score; //player's score
private int timer; //timer countdown

private PImage Paw, Mouse;

Minigame_2(){
  
  score = 0;
  timer = 600;
  
  frameRate (60);
 
  rng = int(random(1,7));
  
}

void display(){
  
  drawHoles();
  update();
  drawScore();
  drawPaw();
  endMinigame2();
  
  } 

void drawHoles(){
  
//background colour
  background(#E0F4FF); 
  
//draw all the holes the mice can pop out of
  ellipseMode(CENTER); 
  noStroke();
  fill(#505050);
  ellipse (150,250,200,200);
  ellipse (400,250,200,200);
  ellipse (650,250,200,200);
  ellipse (150,500,200,200);
  ellipse (400,500,200,200);
  ellipse (650,500,200,200);
  
}

void drawMice(float tempX, float tempY) {
  
  miceX = tempX;
  miceY = tempY;

//draw mouse
  imageMode (CORNER);
  image (loadImage("Assets/mouse.png"), miceX,miceY,150,200);
  
}

void drawPaw(){
  
//draw paw that follows pointer  
  image (loadImage("Assets/catPaw.png"), mouseX-200, mouseY-100);
  
}

void drawScore(){
  
  fill (#FFFFFF);
  textSize (30);
  textAlign (LEFT);
  text ("Score: " +score, 10, 30);
  
}

void drawGameOver(){
  
     fill (#FFDEF3);
     rectMode (CENTER);
     rect (400, 400, 800, 800);
     
     textAlign (CENTER, CENTER);
     fill (#FFFFFF);
     textSize (50);
     text ("Times Up!", 400, 300);
     textSize (30);
     text ("Your score: " +score, 400, 400);
     text ("Click anywhere to continue", 400, 450);  
}

void update(){
  
//draws a mouse in a random hole
  if (rng == 1) {
    drawMice(75, 100);
    }
  if (rng == 2) {
    drawMice(325, 100);
    }
  if (rng == 3) {
    drawMice(575, 100);
    }
  if (rng == 4) {
    drawMice(75, 350);
    }
  if (rng == 5) {
    drawMice(325, 350);
    }
  if (rng == 6) {
    drawMice(575, 350);
    }  
  
}

void click(){
  
//if mouse is clicked, then add score  
  if (mouseX>=miceX-110 && mouseX<=miceX+110 && mouseY>=miceY-125 && mouseY<=miceY+125) {
    score++;
    rng = int(random(1, 7));
 
  }
//clicking on game over screen will take you back to the main pet screen
  else if (timer <= 0){
    gameState = 0;
  }

}

void startMinigame2(){
  
//Start the minigame when clicked on the main screen
  score = 0;
  timer = 600;
  
  frameRate (60);
 
  rng = int(random(1,7));
  
}

void endMinigame2(){
  
//ends the minigame if the time goes down to zero and rewards the player food based on their score  
  if (timer <= 0){
    drawGameOver();
    reward(int(score));
    gameState = 0;
    
  }else
    timer--;
  
}


}
