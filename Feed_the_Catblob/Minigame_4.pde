//Author: Martin Zinko
//Minigame: Memory Card

class Minigame_4{
  
private float Card1 = 0;
private float Card2 = 0;
private float Card1A = 1;
private float Card2A = 1;
private float Card3A = 1;
private float Card4A = 1;
private float Card5A = 1;
private float Card6A = 1;
private float Card7A = 1;
private float Card8A = 1;
private float Card9A = 1;
private float Card10A = 1;
private float score = 0;
private boolean Face1; 
private boolean Face2 = false;
private boolean Face3 = false;
private boolean Face4 = false;
private boolean Face5 = false;
private boolean Face6 = false;
private boolean Face7;
private boolean Face8 = false;
private boolean Face9 = false;
private boolean Face10 = false;
private boolean Clickabl = true;
private boolean Card1ANull = false;
private boolean Card2ANull = false;
private boolean Card3ANull = false;
private boolean Card4ANull = false;
private boolean Card5ANull = false;
private boolean Card6ANull = false;
private boolean Card7ANull = false;
private boolean Card8ANull = false;
private boolean Card9ANull = false;
private boolean Card10ANull = false;

void setup(){
size(800, 800);
frameRate(60);

Card1 = 0;
Card2 = 0;
Card1A = 1;
Card2A = 1;
Card3A = 1;
Card4A = 1;
Card5A = 1;
Card6A = 1;
Card7A = 1;
Card8A = 1;
Card9A = 1;
Card10A = 1;
score = 0;
Face1 = false; 
Face2 = false;
Face3 = false;
Face4 = false;
Face5 = false;
Face6 = false;
Face7 = false;
Face8 = false;
Face9 = false;
Face10 = false;
Clickabl = true;
Card1ANull = false;
Card2ANull = false;
Card3ANull = false;
Card4ANull = false;
Card5ANull = false;
Card6ANull = false;
Card7ANull = false;
Card8ANull = false;
Card9ANull = false;
Card10ANull = false;

}



void update(){ 
  if (score == 5){
   gameState = 0;
   reward(15);
  }
  }

void display(){
  background(255, 154, 162); 
  fill(#DFE0B7);
  Clicks();
  CardPosition();
  Logic();
  
  fill(0);
  textSize(24);
  text("Please don't spam click, it'll break the game...:)", 50, 700, 700, 70);
}

void Clicks(){
if  (Clickabl == true){
//5
  if ((mouseX > 50) && (mouseX < 175) && (mouseY > 100) && (mouseY < 350)&&mousePressed&&(Card1ANull == false)) {
  Card1A = 0;
  mouseReleased();
  if (Card1 == 0){
   Card1 = 5; 
   Face1 = true;
  }
  
  if ((Face1 == false) && (Card1 > 0)) {
   Card2 = 5;
  }
  }
//1
  if ((mouseX > 200) && (mouseX < 325) && (mouseY > 100) && (mouseY < 350)&&mousePressed&&(Card2ANull == false)) {
  Card2A = 0;
  mouseReleased();
  if (Card1 == 0){
   Card1 = 1; 
   Face2 = true;
  }

  if ((Face2 == false) && (Card1 > 0)) {
   Card2 = 1;
  }
  } 
//3
  if ((mouseX > 350) && (mouseX < 475) && (mouseY > 100) && (mouseY < 350)&&mousePressed&&(Card3ANull == false)) {
  Card3A = 0;
  mouseReleased();
  if (Card1 == 0){
   Card1 = 3; 
   Face3 = true;
  }
  
  if ((Face3 == false) && (Card1 > 0)) {
   Card2 = 3;
  }
  }
//4
  if ((mouseX > 500) && (mouseX < 625) && (mouseY > 100) && (mouseY < 350)&&mousePressed&&(Card4ANull == false)) {
    Card4A = 0;
  mouseReleased();
  if (Card1 == 0){
   Card1 = 4; 
   Face4 = true;
  }
  
  if ((Face4 == false) && (Card1 > 0)) {
   Card2 = 4;
  }
  }
//2
  if ((mouseX > 650) && (mouseX < 775) && (mouseY > 100) && (mouseY < 350)&&mousePressed&&(Card5ANull == false)) {
    Card5A = 0;
  mouseReleased();
  if (Card1 == 0){
   Card1 = 2; 
   Face5 = true;
  }
  
  if ((Face5 == false) && (Card1 > 0)) {
   Card2 = 2; 
  }
  }
//4
  if ((mouseX > 50) && (mouseX < 175) && (mouseY > 400) && (mouseY < 650)&&mousePressed&&(Card6ANull == false)) {
    Card6A = 0;
  mouseReleased();
  if (Card1 == 0){
   Card1 = 4; 
   Face6 = true;
  }
  
  if ((Face6 == false) && (Card1 > 0)) {
   Card2 = 4;
  }
  }
//5
  if ((mouseX > 200) && (mouseX < 325) && (mouseY > 400) && (mouseY < 650)&&mousePressed&&(Card7ANull == false)) {
    Card7A = 0;
  mouseReleased();
  if (Card1 == 0){
   Card1 = 5; 
   Face7 = true;
  }
  
  if ((Face7 == false) && (Card1 > 0)) {
   Card2 = 5; 
  }
  }
//2
  if ((mouseX > 350) && (mouseX < 475) && (mouseY > 400) && (mouseY < 650)&&mousePressed&&(Card8ANull == false)) {
    Card8A = 0;
  mouseReleased();
  if (Card1 == 0){
   Card1 = 2; 
   Face8 = true;
  }
  
  if ((Face8 == false) && (Card1 > 0)) {
   Card2 = 2;
  }
  }
//1
  if ((mouseX > 500) && (mouseX < 625) && (mouseY > 400) && (mouseY < 650)&&mousePressed&&(Card9ANull == false)) {
    Card9A = 0;
  mouseReleased();
  if (Card1 == 0){
   Card1 = 1; 
   Face9 = true;
  }
  
  if ((Face9 == false) && (Card1 > 0)) {
   Card2 = 1;
  }
  }
//3
  if ((mouseX > 650) && (mouseX < 775) && (mouseY > 400) && (mouseY < 650)&&mousePressed&&(Card10ANull == false)) {
    Card10A = 0;
  mouseReleased();
  if (Card1 == 0){
   Card1 = 3; 
   Face10 = true;
  }
  
  if ((Face10 == false) && (Card1 > 0)) {
   Card2 = 3;
  }
  }
}
}

void CardPosition() {
  imageMode(CORNER);
//Card1A, face 5. 
  if (Card1A == 1){
   image (loadImage("Assets/cardBack.png"), 50,100,125,250);
  }
  if (Card1A == 0){
    image (loadImage("Assets/card5.png"), 50,100,125,250);
  }
//Card2A, face 1.
  if (Card2A == 1){
  image (loadImage("Assets/cardBack.png"), 200,100,125,250);
  }
  if (Card2A == 0){
  image (loadImage("Assets/card1.png"), 200,100,125,250);
  }
//Card3A, face 3.
  if (Card3A == 1){
  image (loadImage("Assets/cardBack.png"), 350,100,125,250);
  }
  if (Card3A == 0){
  image (loadImage("Assets/card3.png"), 350,100,125,250);
  }
//Card4A, face 4.
  if(Card4A == 1){
  image (loadImage("Assets/cardBack.png"), 500,100,125,250);
  }
  if(Card4A == 0){
  image (loadImage("Assets/card4.png"), 500,100,125,250);
  }  
//Card5A, face 2.
  if(Card5A == 1){
  image (loadImage("Assets/cardBack.png"), 650,100,125,250);
  }
  if(Card5A == 0){
  image (loadImage("Assets/card2.png"), 650,100,125,250);
  }
//Card6A, face 4.
  if(Card6A == 1){
  image (loadImage("Assets/cardBack.png"), 50,400,125,250);
  }
  if(Card6A == 0){
  image (loadImage("Assets/card4.png"), 50,400,125,250);
  }
//Card7A, face 5.
  if(Card7A ==1){
  image (loadImage("Assets/cardBack.png"), 200,400,125,250);
  }
  if(Card7A ==0){
  image (loadImage("Assets/card5.png"), 200,400,125,250);
  }
//Card8A, face 2.
  if(Card8A == 1){
  image (loadImage("Assets/cardBack.png"), 350,400,125,250);
  }
  if(Card8A == 0){
  image (loadImage("Assets/card2.png"), 350,400,125,250);
  }
//Card9A, face 1.
  if(Card9A == 1){
  image (loadImage("Assets/cardBack.png"), 500,400,125,250);
  }
  if(Card9A == 0){
  image (loadImage("Assets/card1.png"), 500,400,125,250);
  }
//Card10A, face 3.
  if(Card10A == 1){
  image (loadImage("Assets/cardBack.png"), 650,400,125,250);
  }
  if(Card10A == 0){
  image (loadImage("Assets/card3.png"), 650,400,125,250);
  }
}

void Logic(){
  if ((Card1 > 0)&&(Card2 > 0)){
    Clickabl=false;
    if(Card1 == Card2){ 
        if (Card1 == 1){
        Card2A = 2;
        Card9A =2;
        Card2ANull=true;
        Card9ANull=true;
        Clickabl = true;
        }
        if (Card1 == 2){
        Card5A = 2;
        Card8A =2;
        Card5ANull=true;
        Card8ANull=true;
        Clickabl = true;
        }
        if (Card1 == 3){
        Card3A = 2;
        Card10A =2;
        Card3ANull=true;
        Card10ANull=true;
        Clickabl = true;
        }
        if (Card1 == 4){
        Card4A = 2;
        Card6A =2;
        Card4ANull=true;
        Card6ANull=true;
        Clickabl = true;
        }
        if (Card1 == 5){
        Card1A = 2;
        Card7A =2;
        Card1ANull=true;
        Card7ANull=true;
        Clickabl = true;
        }
        
        Card1 = 0;
        Card2 = 0;
        score = score + 1;
        
        
   
    }
    //resetting all of the card either back to facedown or leave them invisable
    else {
 
        if((Card1A == 0)||(Card1A == 1)){
        Card1A = 1;
        }
        else {
        Card1A = 2; 
        }
        
        if((Card2A == 0)||(Card2A == 1)){
        Card2A = 1;
        }
        else {
        Card2A = 2; 
        }
        
        if((Card3A == 0)||(Card3A == 1)){
        Card3A = 1;
        }
        else {
        Card3A = 2; 
        }
        
        if((Card4A == 0)||(Card4A == 1)){
        Card4A = 1;
        }
        else {
        Card4A = 2; 
        }
        
        if((Card5A == 0)||(Card5A == 1)){
        Card5A = 1;
        }
        else {
        Card5A = 2; 
        }
        
        if((Card6A == 0)||(Card6A == 1)){
        Card6A = 1;
        }
        else {
        Card6A = 2; 
        }

        if((Card7A == 0)||(Card7A == 1)){
        Card7A = 1;
        }
        else {
        Card7A = 2; 
        }
        
        if((Card8A == 0)||(Card8A == 1)){
        Card8A = 1;
        }
        else {
        Card8A = 2; 
        }

        if((Card9A == 0)||(Card9A == 1)){
        Card9A = 1;
        }
        else {
        Card9A = 2; 
        }
        
        if((Card10A == 0)||(Card10A == 1)){
        Card10A = 1;
        }
        else {
        Card10A = 2; 
        }
      Card1 = 0;
      Card2 = 0;
      Clickabl = true; 
    }
    //noLoop();
  }
  
}



//class bracket
}
