
//Author: Kevin Kulendran
//Minigame: Maze run

//Get to the end zone before time runs out!, reaching the end means food for your pet!

//Side Note** For some reason the timer is bugged and wont go down to 0, unfortunately I was not able to find a fix before the deadline.

//Creates class for Minigame_3

class Minigame_3{

//Setup
void setupMinigame3() {
 
  size(800, 800);
  frameRate(60);
  
}
//Draws Minigame_3
 
  void drawMinigame3() {

  //Background Of the Maze
  background(255, 154, 162); 
  noStroke(); 
  
  startTimer = new Timer(20);
  startTimer.countDown();
  fill(0);
  // text(startTimer.getTime(),20,30);

//Interaction between Player and walls  
  player.display();
  player.move(walls);
 
  for(int i = 0; i < walls.length; i++){
    walls[i].update();
  }
 
}
//Class for the Player Icon (Ball)/Pet
  class circle {
 
  float x;
  float y;
 
  circle(float _x, float _y){
    x = _x;
    y = _y;
  }

//Draws pet (circle icon)
  void display(){
    fill(0);
    ellipse(x,y,25,25);
    
  }
//Keyboard Controls  
  void move(wall[] walls){
 
    float possibleX = x;
    float possibleY = y;
 
    if (keyPressed==true) {
 
      if (key=='a') { 
        possibleX= possibleX - 5;
      } 
      if (key=='d') { 
        possibleX = possibleX + 5;
      } 
      if (key=='w') { 
        possibleY = possibleY - 5;
      } 
      if (key=='s') { 
        possibleY = possibleY + 5;
      }
    }

// Creates end game state upon contact with end zone
      if(x > 675 && y >675){          
       reward(10); 
       gameState =0;
       
    }
    
//Collision With Maze walls    
    boolean didCollide = false;
    for(int i = 0; i < walls.length; i++){
      if(possibleX > walls[i].x && possibleX < (walls[i].x + walls[i].w) && possibleY > walls[i].y && possibleY < walls[i].y + walls[i].h){
        didCollide = true;
      }
    }
 
    if(didCollide == false){
      x = possibleX;
      y = possibleY;
    }
  } 
}
//Maze Wall Class
class wall {
    
  float x;
  float y;
  float w;
  float h;
 
  wall(float _x, float _y, float _w, float _h){
    x = _x;
    y = _y;
    w = _w;
    h = _h;
  }

//Maze Attributes 
  void update(){
    fill(181,234,215);
    rect(x,y,w,h);
    
//End Zone
    fill(161,220,240);
    rect(675,675,50,50);
    
//Start And End Text
  textSize(30);
  text("Start", 30,80);
  text("End", 710,760);
  }

}

//Inital 
circle player;
wall[] walls; 
Timer startTimer;

//Creates Ball and Ball Position 
{
   player = new circle(100,125); 

//Array List Of Walls
    walls = new wall[23];

//Vertical Walls
    walls[0] = new wall(25,50,50,700);
    walls[1] = new wall(725,50,50,700);
    walls[2] = new wall(75,225,50,100);
    walls[3] = new wall(125,75,50,100);
    walls[4] = new wall(350,75,50,100);
    walls[5] = new wall(350,225,50,150);
    walls[6] = new wall(550,225,50,150);
    walls[7] = new wall(325,525,50,150);

//Horizontal Walls
    walls[8] = new wall(25,725,750,40);
    walls[9] = new wall(50,50,700,40);
    walls[10] = new wall(125,150,175,40);
    walls[11] = new wall(175,225,200,40);
    walls[12] = new wall(175,350,600,40);
    walls[13] = new wall(475,75,200,50);
    walls[14] = new wall(650,175,100,50);
    walls[15] = new wall(500,225,100,50);
    walls[16] = new wall(25,425,550,50);
    walls[17] = new wall(350,525,325,50);
    walls[18] = new wall(350,625,375,50);
    walls[19] = new wall(125,525,150,50);
    walls[20] = new wall(125,625,150,100);
    walls[21] = new wall(200,525,75,225);
    walls[22] = new wall(625,425,50,150);
 
  } 
//Class for game timer
class Timer {
  
  float Time;

//Timer Constructer for creating a new timer   
  Timer(float set)
  {
    Time= set;
}
  float getTime()// Returns current time
  {
    return(Time);
}
    void setTime(float set) 
  {
    Time = set;
}
    void countDown()
  {
    Time -= 60/frameRate;//Timer working with game framerate
}
 
 
  }
}
