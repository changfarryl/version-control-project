/*

  Author: Farryl Chang
  
  This class is used to create the first minigame: Mash 'n Dash.

*/

class Minigame_1{
  
  // Properties
  private int distance; // Stores how far the pet has travelled based on how many times the player has clicked the A key
  private int time; // Stores the remaining time the player has before the minigame is over
  private boolean keyDown; // Stores whether the player is holding down the corresponding key ('A' key)
  private int animationTimer; // Controls the running animation
  
  // Minigame_1 constructor
  Minigame_1(){
  
  }
  
  // Updates the minigame
  void update(){
    
    // Limit the distance at 120
    if(distance > 120)
      distance = 120;
    
    // Decrement the remaining time by 1
    time -= 1;
    
    // Decrement the animation timer by 1
    if(animationTimer > 0)
      animationTimer--;
    
    // If the time limit is up, end the minigame
    if(time == 0)
      endMinigame();
    
  }
  
  // Displays the minigame
  void display(){
    
    // Draw the background for minigame 1
    background(245, 213, 140);
    
    // Draw the pet on the screen with its horizontal position being based on how many times the player has clicked the A key
    imageMode(CORNER);
    if(animationTimer > 0)
      image(loadImage("Assets/run2.png"), 50 + 5 * distance, 400, 200, 200);
    else
      image(loadImage("Assets/run1.png"), 50 + 5 * distance, 400, 200, 200);
      
    // Draw the instructions
    rectMode(CENTER);
    fill(255);
    noStroke();
    rect(400, 200, 90 + 4 * sin(frameCount), 90 + 4 * sin(frameCount));
    textSize(64);
    textAlign(CENTER);
    fill(160);
    text("A", 400, 200, 50, 80);
    
    // Display the player's score
    textSize(32);
    text("Distance: " + distance + "m", 400, 700, 300, 80);
    
  }
  
  // Starts the minigame when the player clicks the "Minigame 1" button on the idle screen
  void startMinigame(){
    
    // Initialize values when the minigame starts
    distance = 0;
    time = 120;
    keyDown = false;
    animationTimer = 0;
    
  }
  
  // Ends the minigame when the time is up
  private void endMinigame(){
    
    // Reward the player with food based on the distance
    reward(int(distance));
    
    // TO DO: hide the minigame screen and return to the idle screen
    gameState = 0;
    
  }
  
  // Runs when the player clicks the corresponding key ('A' key)
  void tap(){
    
    // If the key isn't already down, then increment distance
    if(keyDown == false){
      
      distance += 1;
      keyDown = true;
      animationTimer = 3;
    
    }
    
  }
  
  // Runs when the player releases the corresponding key ('A' key)
  void untap(){
    
    // Reset keyDown to allow for next click
    keyDown = false;
    
  }
  
}
