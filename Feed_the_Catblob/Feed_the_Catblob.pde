/*

  Authors: Martin Zinko, Kevin Kulendran, Rei Zhang, Farryl Chang, Cara Wang
  Course: GAME12805 - Programming for Game Designers 1: Fundamentals
  Instructor: Blair Leggett
  Date: November 18, 2019

  Program Title: Feed the Catblob
  Description: This program is a game created using verion control principles. In this game, the player cares for a catblob
               by feeding it with food earned from playing various minigames. Failing to earn enough food to feed the catblob
               will cause it to die of hunger. Keep taking care of the catblob to see it grow into an adult and leave.
  
  Additional Notes: Currently, the fourth minigame has some bugs: occasionally, the cards will not disappear even when two
                    matching cards have been flipped.

*/

// Game state variables
Pet pet; // Stores the player's pet
int dayNum; // Stores the current day number
int lastDay; // Stores the final day number
int food; // Stores the player's total earned food amount
int gameState; // Stores the current game state; 0 - idle screen; 1 - minigame one; 2 - minigame two; 3 - minigame three; 4 - minigame four, 5 - end screen
boolean nextDayAvailable; // Determines if the player may proceed to the next day
int playerReward; // Stores the player's reward
int rewardOpacity; // Controls the opacity at which the reward is displayed

// Game objects
Button[] buttons; // Buttons that will perform various actions when clicked
Minigame_1 minigame1; // Plays the first minigame: Mash 'n Dash
Minigame_2 minigame2; // Plays the second minigame: Whack-a-Mouse
Minigame_3 minigame3; // Plays the third minigame: Maze Run
Minigame_4 minigame4; // Plays the fourth minigame: Memory Card

void setup(){
  
  // Set up the game space
  size(800, 800);
  
  // Initialize game state variables
  pet = new Pet(0, 1);
  dayNum = 1;
  lastDay = 8;
  food = 0;
  gameState = 0;
  nextDayAvailable = false;
  
  // Create minigames
  minigame1 = new Minigame_1();
  minigame2 = new Minigame_2();
  minigame3 = new Minigame_3();
  minigame4 = new Minigame_4();
  
  
  // Create buttons
  buttons = new Button[7]; // 0 - feed pet; 1 to 4 - play corresponding minigame; 5 - proceed to next day; 6 - play again 
  buttons[0] = new Button(350, 540, 450, 640, loadImage("Assets/feed button.png"), 100, 100);
  buttons[1] = new Button(40, 700, 180, 770, loadImage("Assets/button1.png"), 140, 70);
  buttons[2] = new Button(220, 700, 360, 770, loadImage("Assets/button2.png"), 140, 70);
  buttons[3] = new Button(400, 700, 540, 770, loadImage("Assets/button3.png"), 140, 70);
  buttons[4] = new Button(580, 700, 720, 770, loadImage("Assets/button4.png"), 140, 70);
  buttons[5] = new Button(600, 40, 720, 160, loadImage("Assets/next day button.png"), 120, 120);
  buttons[6] = new Button(600, 700, 740, 770, loadImage("Assets/play again.png"), 140, 70);
  
}

void draw(){
  
  // If the player is on the idle screen, update and display the idle screen
  if(gameState == 0){
    
    // Display the idle screen
    idleScreen();
    
  // If the player is on minigame 1, update and display minigame 1
  }else if(gameState == 1){
    
    minigame1.update();
    minigame1.display();
    
  // If the player is on minigame 2, update and display minigame 2
  }else if(gameState == 2){
    
    minigame2.display();
    
  // If the player is on minigame 3, update and display minigame 3
  }else if(gameState == 3){
    
    minigame3.drawMinigame3();
    
  // If the player is on minigame 4, update and display minigame 4
  }else if(gameState == 4){
    minigame4.update();
    minigame4.display();
    
  // If the player is on the end screen, update and display the end screen
  }else if(gameState == 5){
    
    // If the pet has become mature and is not starved, then the player got the good end
    if(pet.getHunger() < 100)
      goodEnd();
    
    // If the pet has starved, then the player got the bad end
    else if(pet.getHunger() >= 100)
      badEnd();
      
    buttons[6].display();
    
  }
  
}

// Displays the idle screen
void idleScreen(){
  
  // Draw the background
  background(255, 217, 238);
  
  // Update and display the pet
  pet.update();
  pet.display();
  
  // Only show the "Feed" button if the player has food to feed and if the pet is hungry
  if(food > 0 && pet.getHunger() > 0)
    buttons[0].display();
  
  // If the player has played a minigame that day, display "Next Day" button
  if(nextDayAvailable) 
    buttons[5].display();
    
  // Otherwise, display every button except the "Next Day" button
  else{
    
    for(int count = 1; count < buttons.length - 2; count++)
      buttons[count].display();
    
  }
    
  // Display the current day number and the player's current food amount
  rectMode(CORNER);
  textSize(32);
  textAlign(LEFT);
  fill(160);
  text("Day " + dayNum + "/" + lastDay, 50, 100, 200, 50);
  text("Food " + food, 50, 170, 200, 50);
  text("Hunger " + pet.getHunger() + "%", 50, 240, 200, 50);
  
  // Tell the player if they have earned rewards
  if(rewardOpacity > 0)
    rewardOpacity -= 1;
  displayReward();
  
}

// Feeds the pet
void feed(){
  
  // If the player has food and if the pet is hungry, then feed the pet
  if(food > 0 && pet.getHunger() > 0){
    
    food--;
    pet.eat();
    
  }
  
}

// Plays the first minigame
void playMinigame1(){
  
  // Start the minigame
  minigame1.startMinigame();
  gameState = 1;
  
}

// Plays the second minigame
void playMinigame2(){
  
  // Start the minigame
  minigame2.startMinigame2();
  gameState = 2;
  
}

// Plays the third minigame
void playMinigame3(){
  
  // Start the minigame
  gameState = 3;
  minigame3 = new Minigame_3();
  
}

// Plays the fourth minigame
void playMinigame4(){
  
  // Start the minigame
  minigame4.setup();
  gameState = 4;
  
}

// Proceeds to the next day
void nextDay(){
  
  // Update game values and increase pet's hunger based on its age
  dayNum++;
  pet.setAge(pet.getAge() + 1);
  pet.setHunger(pet.getHunger() + 8 + 2 * pet.getAge());
  nextDayAvailable = false;
  
  // If the pet has become mature or has starved, end the game
  if(pet.getAge() > lastDay || pet.getHunger() >= 100)
    gameState = 5;
  
}

// Displays the good ending screen
void goodEnd(){
  
  // Display the good ending screen
  imageMode(CORNER);
  image(loadImage("Assets/vicScreen.png"), 0, 0, 800, 800);
  
  // Win message
  fill(0, 255);
  text("WIN?", 630, 660, 100, 50);
  
}

// Displays the bad ending screen
void badEnd(){
  
  // Display the bad ending screen
  imageMode(CORNER);
  image(loadImage("Assets/dedCatto.jpg"), 0, 0, 800, 800);
  
  // Lose message
  fill(255, 255);
  text("LOSE...", 630, 660, 100, 50);
  
}

// Rewards the player with food
void reward(int amount){
    
  // Limit the amount of food the player can keep at 100
  if(food + amount > 100){
    
    amount = 100 - food;
    food = 100;
    
  }else
    food += amount;
    
  // Increase the pet's hunger based on its age
  if(pet.getHunger() + pet.getAge() < 100)
    pet.setHunger(pet.getHunger() + pet.getAge());
  else
    pet.setHunger(99);
    
  // Display the player's rewards
  playerReward = amount;
  rewardOpacity = 100;
  displayReward();
    
  // Allow the player to proceed to the next day
  nextDayAvailable = true;
  
}

// Displays the player's reward
void displayReward(){
  
  // Display the reward, with opacity decreasing over time
  textSize(32);
  textAlign(LEFT);
  fill(0, rewardOpacity);
  text("+" + playerReward, 200, 170, 200, 50);
  
}

// Performs an action depending on which button was clicked
void buttonClickCheck(){
  
  // If the 'Feed' button is clicked, feed the pet
  if(buttons[0].mouseCheck() && gameState == 0)
    feed();
    
  // If the 'Minigame 1' button is clicked, play the first minigame
  else if(buttons[1].mouseCheck() && nextDayAvailable == false && gameState == 0)
    playMinigame1();
    
  // If the 'Minigame 2' button is clicked, play the second minigame
  else if(buttons[2].mouseCheck() && nextDayAvailable == false && gameState == 0)
    playMinigame2();
    
  // If the 'Minigame 3' button is clicked, play the third minigame
  else if(buttons[3].mouseCheck() && nextDayAvailable == false && gameState == 0)
    playMinigame3();
    
  // If the 'Minigame 4' button is clicked, play the fourth minigame
  else if(buttons[4].mouseCheck() && nextDayAvailable == false && gameState == 0)
    playMinigame4();
    
  // If the 'Next Day' button is clicked, proceed to the next day
  else if(buttons[5].mouseCheck() && nextDayAvailable && gameState == 0)
    nextDay();
    
  // If the "Play Again" button is clicked, restart the game
  else if(buttons[6].mouseCheck() && gameState == 5)
    setup();
  
}

// Receives mouse input and runs the corresponding mouse event
void mousePressed(){
  
  // If the player is on the idle screen or on the end screen, run the button check
  if(gameState == 0 || gameState == 5)
    buttonClickCheck();
  
  if(gameState == 2)
    minigame2.click();
}

// Receives keyboard input and runs the corresponding key event
void keyPressed(){
  
  // Perform the corresponding method based on keyboard input
  if(keyCode == 65)
    minigame1.tap();
  
}

// Receives keyboard release and runs the corresponding key event
void keyReleased(){
  
  // Perform the corresponding method based on the key released
  if(keyCode == 65)
    minigame1.untap();
  
}
